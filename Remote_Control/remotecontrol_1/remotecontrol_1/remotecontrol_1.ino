//
// remotecontrol_1
//
// Description of the project
// Developed with [embedXcode](http://embedXcode.weebly.com)
//
// Author 		王昶文
// 				王昶文
//
// Date			2017/1/27 15:48
// Version		<#version#>
//
// Copyright	© 王昶文, 2017年
// Licence		<#licence#>
//
// See         ReadMe.txt for references
//


// Core library for code-sense - IDE-based
#if defined(WIRING) // Wiring specific
#include "Wiring.h"
#elif defined(MAPLE_IDE) // Maple specific
#include "WProgram.h"
#elif defined(MPIDE) // chipKIT specific
#include "WProgram.h"
#elif defined(DIGISPARK) // Digispark specific
#include "Arduino.h"
#elif defined(ENERGIA) // LaunchPad specific
#include "Energia.h"
#elif defined(LITTLEROBOTFRIENDS) // LittleRobotFriends specific
#include "LRF.h"
#elif defined(MICRODUINO) // Microduino specific
#include "Arduino.h"
#elif defined(SPARK) || defined(PARTICLE) // Particle / Spark specific
#include "Arduino.h"
#elif defined(TEENSYDUINO) // Teensy specific
#include "Arduino.h"
#elif defined(REDBEARLAB) // RedBearLab specific
#include "Arduino.h"
#elif defined(ESP8266) // ESP8266 specific
#include "Arduino.h"
#elif defined(ARDUINO) // Arduino 1.0 and 1.5 specific
#include "Arduino.h"
#else // error
#error Platform not defined
#endif // end IDE

// Set parameters


// Include application, user and local libraries


// Define structures and classes


// Define variables and constants


// Prototypes


// Utilities


// Functions




int ledout = 11;
int micin = A0;
int newclap=0, lastclap=0;
bool trigger=false;
float val=0;
int claptime=0;

// Add setup code
void setup()
{
    Serial.begin(9600);
    pinMode(ledout, OUTPUT);
}

// Add loop code
void loop()
{
    if (trigger)
        digitalWrite(ledout, HIGH);
    else
        digitalWrite(ledout, HIGH);
    
    val = analogRead(micin);
    Serial.println(val);
    
    //get the clap time now
    if (val>=500) {
        newclap=millis();
    }
    
    if (lastclap!=0) {
        claptime=newclap-lastclap;
        if (claptime>=300&&claptime<=800) {
            trigger=!trigger;
        }
    }
    
    
    lastclap = newclap;
}



















