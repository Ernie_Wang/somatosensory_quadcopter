#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"

MPU6050 MPU6050;

float ProllConst = 0;               //roll P-controller常數
float IrollConst = 0;              //roll I-controller常數
float DrollConst = 0;              //roll D-controller常數
int pid_max_roll = 0;                    //Maximum output of the PID-controller (+/-)

float PpitchConst = ProllConst;  //pitch P-controller.常數
float IpitchConst = IrollConst;  //pitch I-controller.常數
float DpitchConst = DrollConst;  //pitch D-controller.常數
int pid_max_pitch = pid_max_roll;          //Maximum output of the PID-controller (+/-)

float PyawConst = 0;                //yaw P-controller.常數 //4.0
float IyawConst = 0;               //yaw I-controller.常數 //0.02
float DyawConst = 0;                //yaw D-controller.常數
int pid_max_yaw = 0;                     //Maximum output of the PID-controller (+/-)

int acc_axis[4], gyro_axis[4];
int calTime;

double gyro_axis_cal[4];
double gyro_pitch, gyro_roll, gyro_yaw;

float angle_pitch, angle_roll;

float roll_setpoint =0, pitch_setpoint =0; //設定PID基準點
float pitch_setpoint =0;
float yaw_setpoint ;

float Error;
float pid_i_mem_roll, gyro_roll_input, PIDRoll,LastDRollError;
float pid_i_mem_pitch, gyro_pitch_input, PIDPitch, LastDPitchError;
float pid_i_mem_yaw, gyro_yaw_input, PIDYaw, LastDYawError;
float angle_roll_acc, angle_pitch_acc;
boolean gyro_angles_set;

void setup() 
{
  Wire.begin();
  Serial.begin(38400);
  Serial.println("設定MPU6050中");
  MPU6050.initialize();
  MPU6050.setFullScaleGyroRange(MPU6050_GYRO_FS_500); //陀螺儀設定+-500度
  MPU6050.setFullScaleAccelRange(MPU6050_ACCEL_FS_8); //加速計設定+-8G

  for (calTime = 0; calTime < 2000 ; calTime ++)
  {                           
    MPU6050_Info();                                                        //讀取MPU6050
    gyro_axis_cal[1] += gyro_axis[1];
    gyro_axis_cal[2] += gyro_axis[2];
    gyro_axis_cal[3] += gyro_axis[3];
  }
  //取得2000筆數據的總和後，除以2000以求得平均值
  gyro_axis_cal[1] /= 2000;
  gyro_axis_cal[2] /= 2000;
  gyro_axis_cal[3] /= 2000;

  Serial.println("MPU6050 Ready");

  roll_setpoint = 0;
  pitch_setpoint = 0;
  yaw_setpoint = 0;
}

void loop() 
{
  MPU6050_Info();                                                        //讀取MPU6050
  calculatePID();
  
}

void calculatePID()
{
    //Roll PID計算
  Error =gyro_roll_input - roll_setpoint;
  pid_i_mem_roll += IrollConst * Error;                    //(PID I累積誤差 I常數*誤差值)
  if(pid_i_mem_roll > pid_max_roll)pid_i_mem_roll = pid_max_roll;
  else if(pid_i_mem_roll < pid_max_roll * -1)pid_i_mem_roll = pid_max_roll * -1;

  PIDRoll = ProllConst * Error + pid_i_mem_roll + DrollConst * (Error -LastDRollError);
    //pid 修正 roll:        P(P常數*誤差量)             +I(I常數*累積誤差)+ D
  if(PIDRoll > pid_max_roll)PIDRoll = pid_max_roll;
  else if(PIDRoll < pid_max_roll * -1)PIDRoll = pid_max_roll * -1;

 LastDRollError = Error;

  //Pitch PID計算
  Error = gyro_pitch_input - pitch_setpoint;
  pid_i_mem_pitch += IpitchConst * Error;                    //(PID I累積誤差 I常數*誤差值)
  if(pid_i_mem_pitch > pid_max_pitch)pid_i_mem_pitch = pid_max_pitch;
  else if(pid_i_mem_pitch < pid_max_pitch * -1)pid_i_mem_pitch = pid_max_pitch * -1;

  PIDPitch = PpitchConst * Error + pid_i_mem_pitch + DpitchConst * (Error - LastDPitchError);
  //pid 修正 pitch:        P(P常數*誤差量)             +I(I常數*累積誤差)+ D
  if(PIDPitch > pid_max_pitch)PIDPitch = pid_max_pitch;
  else if(PIDPitch < pid_max_pitch * -1)PIDPitch = pid_max_pitch * -1;

  LastDPitchError = Error;

  //Yaw PID計算
  Error = gyro_yaw_input - yaw_setpoint;
  pid_i_mem_yaw += IyawConst * Error;                    //(PID I累積誤差 I常數*誤差值)
  if(pid_i_mem_yaw > pid_max_yaw)pid_i_mem_yaw = pid_max_yaw;
  else if(pid_i_mem_yaw < pid_max_yaw * -1)pid_i_mem_yaw = pid_max_yaw * -1;

  PIDYaw = PyawConst * Error + pid_i_mem_yaw + DyawConst * (Error - LastDYawError);
    //pid 修正 yaw:        P(P常數*誤差量)             +I(I常數*累積誤差)+ D
  if(PIDYaw > pid_max_yaw)PIDYaw = pid_max_yaw;
  else if(PIDYaw < pid_max_yaw * -1)PIDYaw = pid_max_yaw * -1;

  LastDYawError = Error;
}

void MPU6050_Info()  //取得MPU6050讀到的數值
{
    int16_t ax, ay, az;
    int16_t gx, gy, gz;
    MPU6050.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
    acc_axis[1] = ax;
    acc_axis[2] = ay;
    acc_axis[3] = az;
    gyro_axis[1] = gx;
    gyro_axis[2] = gy;
    gyro_axis[3] = gz;

    //修正讀數
    if(calTime == 2000)
    {
    gyro_axis[1] -= gyro_axis_cal[1];
    gyro_axis[2] -= gyro_axis_cal[2];
    gyro_axis[3] -= gyro_axis_cal[3];
  }
}
