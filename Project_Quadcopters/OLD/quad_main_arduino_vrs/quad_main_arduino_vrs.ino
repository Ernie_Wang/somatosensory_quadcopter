// Core library for code-sense - IDE-based
#if defined(WIRING) // Wiring specific
#include "Wiring.h"
#elif defined(MAPLE_IDE) // Maple specific
#include "WProgram.h"
#elif defined(MPIDE) // chipKIT specific
#include "WProgram.h"
#elif defined(DIGISPARK) // Digispark specific
#include "Arduino.h"
#elif defined(ENERGIA) // LaunchPad specific
#include "Energia.h"
#elif defined(LITTLEROBOTFRIENDS) // LittleRobotFriends specific
#include "LRF.h"
#elif defined(MICRODUINO) // Microduino specific
#include "Arduino.h"
#elif defined(SPARK) || defined(PARTICLE) // Particle / Spark specific
#include "Arduino.h"
#elif defined(TEENSYDUINO) // Teensy specific
#include "Arduino.h"
#elif defined(REDBEARLAB) // RedBearLab specific
#include "Arduino.h"
#elif defined(ESP8266) // ESP8266 specific
#include "Arduino.h"
#elif defined(ARDUINO) // Arduino 1.0 and 1.5 specific
#include "Arduino.h"
#else // error
#error Platform not defined
#endif // end IDE



#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"



////---------------MY PROGRAM START FROM HERE-------------------////








const int motor[] = {3, 6, 9, 11};// 1=>3  2=>6  3=>9  4=>11
bool controled=false, start=false;
int throttle=0;
int countstart=0, countend=0, countdiff=0;
bool throttlezero=false;
bool starting=false;

//    Declaring mpu_6050 global variables
MPU6050 mpu;

// Timers
unsigned long timer1 = 0, timer2;;
float timeStep = 0.075;

// Pitch, Roll and Yaw values
float pitch = 0;
float roll = 0;
float yaw = 0;


//    PID global valiables


float ProllConst = 0.5;               //roll P-controller常數
float IrollConst = 0.4;              //roll I-controller常數
float DrollConst = 0.1;              //roll D-controller常數
int pid_max_roll = 1000;                    //Maximum output of the PID-controller (+/-)

float PpitchConst = ProllConst;  //pitch P-controller.常數
float IpitchConst = IrollConst;  //pitch I-controller.常數
float DpitchConst = DrollConst;  //pitch D-controller.常數
int pid_max_pitch = pid_max_roll;          //Maximum output of the PID-controller (+/-)

float PyawConst = ProllConst;                //yaw P-controller.常數 //4.0
float IyawConst = IrollConst;               //yaw I-controller.常數 //0.02
float DyawConst = DrollConst;                //yaw D-controller.常數
int pid_max_yaw = pid_max_roll;                     //Maximum output of the PID-controller (+/-)

int acc_axis[4], gyro_axis[4];
int calTime;

double gyro_axis_cal[4];
double gyro_pitch, gyro_roll, gyro_yaw;

float angle_pitch, angle_roll, angle_yaw;

float roll_setpoint =0, pitch_setpoint =0; //設定PID基準點
float yaw_setpoint=0 ;

float Error;
float pid_i_mem_roll, gyro_roll_input, PIDRoll,LastDRollError;
float pid_i_mem_pitch, gyro_pitch_input, PIDPitch, LastDPitchError;
float pid_i_mem_yaw, gyro_yaw_input, PIDYaw, LastDYawError;
float angle_roll_acc, angle_pitch_acc;
boolean gyro_angles_set;



//--------------------------------------------------------//

//radio globle variable
RF24 radio(7,8);

float Data[5];
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };





//-------------------------------------------------------//

//function name
void gyro_cal();
void PID_cal();

// Add setup code
void setup()
{
    //   start set up mpu_6050
    Wire.begin();             //Start I2C as master
    Serial.begin(115200);      //Use only for debugging
    pinMode(13, OUTPUT);      //Set output 13 (LED) as output
    
    
    ////-----set up gyro
    while(!mpu.begin(MPU6050_SCALE_2000DPS, MPU6050_RANGE_2G))
    {
        Serial.println("Could not find a valid MPU6050 sensor, check wiring!");
        delay(500);
    }
    
    // Calibrate gyroscope. The calibration must be at rest.
    // If you don't want calibrate, comment this line.
    mpu.calibrateGyro();
    
    // Set threshold sensivty. Default 3.
    // If you don't want use threshold, comment this line or set 0.
    mpu.setThreshold(3);
    
    
    ////-----set radio
    
    radio.begin();
    radio.setRetries(15,15);
    radio.openWritingPipe(pipes[1]);
    radio.openReadingPipe(1,pipes[0]);
    radio.setPALevel(RF24_PA_MIN);
    radio.setDataRate(RF24_1MBPS);
    
    
    
    
    digitalWrite(13, LOW);                    //All done, turn the LED off
    
    
    
    
    
}

// Add loop code
void loop()
{
    read_mpu_6050_data();
    
    timer2=millis();
    timeStep=(timer2-timer1)*0.001;
    timer1 =millis();
    
    gyro_cal();
    
    starting=false;
    radio.startListening();
    if (radio.available()) {
        radio.read(&Data ,sizeof(Data));
        //ok
        if (Data[0]==0&&Data[1]==0) {
            //to start
            if (throttle==0) {
                if (throttlezero==false) {        //judge whether *countstart  *countend is counting for the shut down program
                    if (countstart==0) {
                        countstart=millis();      //record the time of countstart;
                    }
                    countend=millis();            //record the time of countend;
                    countdiff=countend-countstart;
                    if (countdiff>=2500) {        //when the difference between countstart and countend is 2.5 second
                        start=true;               //you can start to control coptors
                        countstart=0;
                        countend=0;
                        throttle=1;
                        //first_timer = micros();                   //Reset the loop timer
                        analogWrite(motor[1], 200);
                        analogWrite(motor[1], 0);
                    }
                }
                else {                            //if the countstart and countend is counting for the shut down, reset to zero
                    throttlezero=false;
                    countstart=0;
                    countend=0;
                }
                starting=true;
            }
            //emergency stop
            else {
                delay(1000);
                if (radio.available()) {
                    radio.read(&Data ,sizeof(Data));
                    if (Data[0]==0&&Data[1]==0) {
                        for (int i=0; i<4; i++) {
                            analogWrite(motor[i], 0);
                        }
                        start=false;
                    }
                }
            }
        }
        
        
        
        
        if (start) {     //if haven't start, you won't able to start to control
            
            for (int i=0; i<4; i++) {
                analogWrite(motor[i], throttle);
            }
            roll_setpoint=Data[2];
            pitch_setpoint=Data[3];
            yaw_setpoint=Data[4];
            
            
            //remote
            
            
            //the radio will give a set of number
            
            
            
            
            
            
            throttlezero=false;
            //Reset the loop timer
            
            
            /*
             else {
             roll_setpoint =0;
             pitch_setpoint =0;
             }*/
            
            
            //use PID to controll static
            
            
            
        }
        
        //ok
        if (throttle<=3) {     //shut down the coptors for safety   //the logic is same as start
            if (start) {
                if (!starting) {
                    if (throttlezero==true) {
                        if (countstart==0) {
                            countstart=millis();
                        }
                        countend=millis();
                        countdiff=countend-countstart;
                        if (countdiff>=5000) {
                            start=false;
                            countstart=0;
                            countend=0;
                            throttle=0;
                            Serial.println("Engine down ");
                            Serial.print("throttle : ");
                            Serial.println(throttle);
                        }
                    } else {
                        throttlezero=true;
                        countstart=0;
                        countend=0;
                    }
                }
            }
            //first_timer = micros();                   //Reset the loop timer
        }
    }
    else {
        roll_setpoint=0;
        pitch_setpoint=0;
        yaw_setpoint=0;
    }
    
    
    
    
    
    PID_cal();
    
    Serial.print(" PIDPitch = ");
    Serial.print(PIDPitch);
    Serial.print(" PIDRoll = ");
    Serial.print(PIDRoll);
    Serial.print(" PIDYaw = ");
    Serial.println(PIDYaw);
    
}

void PID_cal() {
    
    //Pitch PID計算
    Error = angle_pitch - pitch_setpoint;
    pid_i_mem_pitch += IpitchConst * Error;                    //(PID I累積誤差 I常數*誤差值)
    
    if(pid_i_mem_pitch > pid_max_pitch)
        pid_i_mem_pitch = pid_max_pitch;
    else if(pid_i_mem_pitch < pid_max_pitch * -1)
        pid_i_mem_pitch = pid_max_pitch * -1;
    
    PIDPitch = PpitchConst * Error + pid_i_mem_pitch + DpitchConst * (Error - LastDPitchError);
    
    //pid 修正 pitch:        P(P常數*誤差量)             +I(I常數*累積誤差)+ D
    if(PIDPitch > pid_max_pitch)
        PIDPitch = pid_max_pitch;
    else if(PIDPitch < pid_max_pitch * -1)
        PIDPitch = pid_max_pitch * -1;
    
    LastDPitchError = Error;
    
    
    //Roll PID計算
    Error =angle_roll - roll_setpoint;
    pid_i_mem_roll += IrollConst * Error;                    //(PID I累積誤差 I常數*誤差值)
    if(pid_i_mem_roll > pid_max_roll)
        pid_i_mem_roll = pid_max_roll;
    else if(pid_i_mem_roll < pid_max_roll * -1)
        pid_i_mem_roll = pid_max_roll * -1;
    
    PIDRoll = ProllConst * Error + pid_i_mem_roll + DrollConst * (Error -LastDRollError);
    //pid 修正 roll:        P(P常數*誤差量)             +I(I常數*累積誤差)+ D
    if(PIDRoll > pid_max_roll)
        PIDRoll = pid_max_roll;
    else if(PIDRoll < pid_max_roll * -1)
        PIDRoll = pid_max_roll * -1;
    
    LastDRollError = Error;
    
    
    //Yaw PID計算
    Error = angle_yaw - yaw_setpoint;
    pid_i_mem_yaw += IyawConst * Error;                    //(PID I累積誤差 I常數*誤差值)
    if(pid_i_mem_yaw > pid_max_yaw)
        pid_i_mem_yaw = pid_max_yaw;
    else if(pid_i_mem_yaw < pid_max_yaw * -1)
        pid_i_mem_yaw = pid_max_yaw * -1;
    
    PIDYaw = PyawConst * Error + pid_i_mem_yaw + DyawConst * (Error - LastDYawError);
    //pid 修正 yaw:        P(P常數*誤差量)             +I(I常數*累積誤差)+ D
    
    if(PIDYaw > pid_max_yaw)
        PIDYaw = pid_max_yaw;
    else if(PIDYaw < pid_max_yaw * -1)
        PIDYaw = pid_max_yaw * -1;
    
    LastDYawError = Error;
}

void gyro_cal() {
    
    // Read normalized values
    Vector norm = mpu.readNormalizeGyro();
    
    // Calculate Pitch, Roll and Yaw
    pitch = pitch + norm.YAxis * timeStep;
    roll = roll + norm.XAxis * timeStep;
    yaw = yaw + norm.ZAxis * timeStep;
    
    angle_pitch=pitch;
    angle_roll=roll;
    angle_yaw=yaw;
    
    // Output raw
    Serial.print(" Pitch = ");
    Serial.print(pitch);
    Serial.print(" Roll = ");
    Serial.print(roll);
    Serial.print(" Yaw = ");
    Serial.print(yaw);
    
    
}













