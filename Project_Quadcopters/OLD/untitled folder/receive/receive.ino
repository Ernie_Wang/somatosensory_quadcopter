#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

RF24 radio(7,8);

int Data[6];
volatile short throttle , yaw , roll , pitch;
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };

void setup(void)
{
  Serial.begin(38400);
  radio.begin();
  radio.setRetries(15,15);
  radio.openWritingPipe(pipes[1]);
  radio.openReadingPipe(1,pipes[0]);
  radio.setPALevel(RF24_PA_MIN);
  radio.setDataRate(RF24_1MBPS);

   Serial.println("ready");
}

void loop(void)
{
  delay(4);
  getControl();
    
}
void getControl()
{
  radio.startListening();
  if ( radio.available() )
    {
        radio.read(&Data ,sizeof(Data));
        Serial.print(Data[0]);Serial.print("  ");
        Serial.print(Data[1]);Serial.print("  ");
        Serial.print(Data[2]);Serial.print("  ");
        Serial.print(Data[3]);Serial.print("  ");
        Serial.print(Data[4]);Serial.print("  ");
        Serial.print(Data[5]);Serial.println("  ");
    }
      delay(2);
}


