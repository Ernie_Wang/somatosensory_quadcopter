
// Core library for code-sense - IDE-based
#if defined(WIRING) // Wiring specific
#include "Wiring.h"
#elif defined(MAPLE_IDE) // Maple specific
#include "WProgram.h"
#elif defined(MPIDE) // chipKIT specific
#include "WProgram.h"
#elif defined(DIGISPARK) // Digispark specific
#include "Arduino.h"
#elif defined(ENERGIA) // LaunchPad specific
#include "Energia.h"
#elif defined(LITTLEROBOTFRIENDS) // LittleRobotFriends specific
#include "LRF.h"
#elif defined(MICRODUINO) // Microduino specific
#include "Arduino.h"
#elif defined(SPARK) || defined(PARTICLE) // Particle / Spark specific
#include "Arduino.h"
#elif defined(TEENSYDUINO) // Teensy specific
#include "Arduino.h"
#elif defined(REDBEARLAB) // RedBearLab specific
#include "Arduino.h"
#elif defined(ESP8266) // ESP8266 specific
#include "Arduino.h"
#elif defined(ARDUINO) // Arduino 1.0 and 1.5 specific
#include "Arduino.h"
#else // error
#error Platform not defined
#endif // end IDE

// Set parameters


// Include application, user and local libraries


// Define structures and classes


// Define variables and constants


// Prototypes


// Utilities


// Functions


////----------MY PROGRAM START FROM HERE------------////
const int motor[] = {3, 6, 9, 11};// 1=>3  2=>6  3=>9  4=>11
int redpin = 9, blackpin=5;
int throttle=0;
int countstart=0, countend=0, countdiff=0;
bool throttlezero=false, start=false;
bool redstatus=0, blackstatus=0;
bool starting;

// Add setup code
void setup()
{
    
    Serial.begin(9600);
    pinMode(redpin, INPUT_PULLUP);
    pinMode(blackpin, INPUT_PULLUP);
    
    
}

// Add loop code
void loop()
{
    starting=false;
    redstatus=digitalRead(redpin);
    blackstatus=digitalRead(blackpin);
    /*
    Serial.println(redstatus);
    Serial.println(blackstatus);
    Serial.print("countstart : ");
    Serial.println(countstart);
    Serial.print("countdiff : ");
    Serial.println(countdiff);
    Serial.println(" ");
    Serial.print("throttlezero : ");
    Serial.println(throttlezero);
    delay(1000);
    */
    if (throttle==0) {
        if ( blackstatus==1&&redstatus==1     /* the countrol buttom*2 */) {    //tell the coptors to start
            //Serial.println(throttlezero);
            if (throttlezero==false) {        //judge whether *countstart  *countend is counting for the shut down program
                
                if (countstart==0) {
                    countstart=millis();      //record the time of countstart;
                }
                countend=millis();            //record the time of countend;
                countdiff=countend-countstart;
                if (countdiff>=2500) {        //when the difference between countstart and countend is 2.5 second
                    start=true;               //you can start to control coptors
                    countstart=0;
                    countend=0;
                    Serial.println("Engine start ");
                    throttle=1;
                    //delay(10000);
                }
            }
            else {                            //if the countstart and countend is counting for the shut down, reset to zero
                throttlezero=false;
                countstart=0;
                countend=0;
            }
            starting=true;
        }

    }
    
    if (start) {
        if (blackstatus==0&&redstatus==1&&throttle<=253) {
            throttle+=2;
            Serial.print("throttle : ");
            Serial.println(throttle);
            throttlezero=false;
        }
        if (blackstatus==1&&redstatus==0&&throttle>=3) {
            throttle-=2;
            Serial.print("throttle : ");
            Serial.println(throttle);
            throttlezero=false;
        }
        
        analogWrite(motor[0], throttle);

    }
    
    if (throttle<=3) {            //shut down the coptors for safety   //the logic is same as start
        if (start) {
            if (!starting) {
                if (throttlezero==true) {
                    if (countstart==0) {
                        countstart=millis();
                    }
                    countend=millis();
                    countdiff=countend-countstart;
                    if (countdiff>=5000) {
                        start=false;
                        countstart=0;
                        countend=0;
                        Serial.println("Engine down ");
                        throttle=0;
                        Serial.print("throttle : ");
                        Serial.println(throttle);
                    }
                } else {
                    throttlezero=true;
                    countstart=0;
                    countend=0;
                }
            }
        }
        
    }
    delay(10);
}















