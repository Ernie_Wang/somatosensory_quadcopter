
#include <SPI.h>
#include "RF24.h"

//  RF
#define CE_pin 7
#define CSN_pin 8
#define SCK_pin 13
#define MISO_pin 12
#define MOSI_pin 11
//#define SS_pin 10

#define Channel 30  //頻道 0~127

bool radioNumber = 0; // 0為接收 1為輸出
byte addresses[][6] = {"1Node","2Node"};  //通關暗號

RF24 radio(CE_pin,CSN_pin);

void setup() 
{ 
  Serial.begin(57600);
  Serial.println("RF receiver");
  radio.begin();

  radio.setPALevel(RF24_PA_LOW);    //功率放大器，可選RF24_PA_MIN, RF24_PA_LOW, RF24_PA_HIGH, RF24_PA_MAX, RF24_PA_ERROR
  radio.setDataRate(RF24_2MBPS);    //傳輸速率，可選RF24_1MBPS, RF24_2MBPS, RF24_250KBPS
  radio.setAutoAck(false);          //自動回應，預設為true
  
  radio.setChannel(Channel);
  radio.openWritingPipe(addresses[0]);
  radio.openReadingPipe(1,addresses[1]);
  
  radio.startListening();

}

void loop() 
{
  unsigned long RX_inf=0;
  Serial.println("again");
  
  if(radio.available()){  //接收到訊號
    radio.read(&RX_inf,sizeof(unsigned long));
    Serial.print("Now response inf: ");
    Serial.println(RX_inf);

    
    //  執行
   
    delay(500);                           // 等待 Servo 旋轉指定的位置  
  }
  

}
